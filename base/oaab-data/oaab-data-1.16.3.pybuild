# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild.info import PV, P

from common.mw import MW, File, InstallDir


class Package(MW):
    DESC = "A free-to-use asset repository for the Morrowind Community."
    HOMEPAGE = "https://github.com/Of-Ash-And-Blight/OAAB-Data"
    LICENSE = "attribution-nc"
    NAME = "OAAB_Data"
    IUSE = "gitd sunrays epic-plants sm-bitter-coast-trees"
    KEYWORDS = "~openmw"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        gitd? ( arch-misc/glow-in-the-dahrk )
        sunrays? ( arch-misc/glow-in-the-dahrk[sunrays=] )
        epic-plants? ( land-flora/epic-plants )
        sm-bitter-coast-trees? ( land-flora/sm-bitter-coast-tree-replacer )
    """
    SRC_URI = f"{HOMEPAGE}/archive/refs/tags/{PV}.zip -> {P}.zip"
    S = f"oaab-data-{PV}/OAAB-Data-{PV}"
    INSTALL_DIRS = [
        InstallDir(
            "00 Core", BLACKLIST=["00 Core/MWSE"], PLUGINS=[File("OAAB_Data.esm")]
        ),
        InstallDir("01 Epic Plants Patch", REQUIRED_USE="epic-plants"),
        InstallDir(
            "02 SM_Bitter Coast Trees Patch", REQUIRED_USE="sm-bitter-coast-trees"
        ),
    ]
