# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Remiros' Groundcover"
    DESC = "Adds groundcover to almost all regions"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46733"
    KEYWORDS = "openmw"
    LICENSE = "attribution-nc"
    RDEPEND = """
        modules/openmw-config[grass]
        base/morrowind[bloodmoon,tribunal]
        tr? ( >=landmasses/tamriel-rebuilt-22.11.02 )
    """
    TEXTURE_SIZES = "512 1024"
    MAIN_FILE = "Remiros'_Groundcover-46733-4-1-1675861420"
    NEXUS_SRC_URI = f"""
        https://www.nexusmods.com/morrowind/mods/46733?tab=files&file_id=1000035688
        -> {MAIN_FILE}.7z
    """
    IUSE = "tr +solstheim minimal mushrooms thick-grass"
    # With minimal enabled, the only things provided are the assets,
    # which are needed for base/tomb-of-the-snow-prince[grass]
    INSTALL_DIRS = [
        InstallDir(
            "00 Core OpenMW",
            GROUNDCOVER=[
                File("Rem_AC.esp", REQUIRED_USE="!minimal"),
                File("Rem_AI.esp", REQUIRED_USE="!minimal"),
                File("Rem_AL.esp", REQUIRED_USE="!minimal"),
                File("Rem_BC.esp", REQUIRED_USE="!minimal"),
                File("Rem_GL.esp", REQUIRED_USE="!minimal"),
                File("Rem_Solstheim.esp", REQUIRED_USE="solstheim !minimal"),
                File("Rem_WG.esp", REQUIRED_USE="!minimal"),
            ],
            S=MAIN_FILE,
        ),
        InstallDir(
            "01a No Mushrooms OpenMW",
            REQUIRED_USE="!mushrooms",
            S=MAIN_FILE,
        ),
        InstallDir(
            "01b Thicker Grass OpenMW",
            REQUIRED_USE="thick-grass",
            S=MAIN_FILE,
        ),
        InstallDir(
            "02 Vanilla Resolution Textures",
            REQUIRED_USE="texture_size_512",
            S=MAIN_FILE,
        ),
        InstallDir(
            "03 TR Plugins",
            GROUNDCOVER=[
                File("Rem_AC_TR.esp"),
                File("Rem_AI_TR.esp"),
                File("Rem_AL_TR.esp"),
                File("Rem_AT_TR.esp"),
                File("Rem_BC_TR.esp"),
                File("Rem_GL_TR.esp"),
                File("Rem_RR_TR.esp"),
                File("Rem_VM_TR.esp"),
                File("Rem_WG_TR.esp"),
            ],
            REQUIRED_USE="tr !minimal",
            S=MAIN_FILE,
        ),
        # InstallDir(
        #     "05a Legend of Chemua",
        #     PLUGINS=[File("Rem_LoC.esp")],
        # ),
        # InstallDir(
        #     "05b Legend of Chemua Moved",
        #     PLUGINS=[File("Rem_LoCM.esp")],
        # ),
    ]
    SETTINGS = {
        "Groundcover": {
            "enabled": "true",
            "density": 1.0,
            "min chunk size": 0.5,
            "stomp mode": 2,
            "stomp intensity": 2,
        }
    }
