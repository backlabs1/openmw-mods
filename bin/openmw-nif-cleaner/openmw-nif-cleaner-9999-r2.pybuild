# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil
import sys

from pybuild import Pybuild2
from pybuild.info import PN

from common.git import Git


def make_exec(path):
    mode = os.stat(path).st_mode
    mode = mode | (mode & 0o444) >> 2
    os.chmod(path, mode)


class Package(Git, Pybuild2):
    NAME = "OpenMW NIF Cleaner"
    DESC = "An application to automatically fix normal maps to work with OpenMW"
    LICENSE = "MIT"
    HOMEPAGE = "https://github.com/treymerkley/openmw-nif-cleaner"
    GIT_SRC_URI = "https://github.com/treymerkley/openmw-nif-cleaner.git"
    S = PN
    RDEPEND = "~dev-python/pyffi-2.2.2"
    PATCHES = "relative_import.patch"

    def src_install(self):
        destpath = os.path.join(self.D, "lib", "python", "onc")
        os.makedirs(destpath)
        for file in os.listdir("."):
            shutil.copy(file, os.path.join(destpath, file))

        wrapper_dir = os.path.join(self.D, "bin")
        os.makedirs(wrapper_dir)
        wrapper_name = "openmw-nif-cleaner"
        if "platform_win32" in self.USE:
            path = os.path.join(wrapper_dir, wrapper_name + ".bat")
            with open(path, "w") as file:
                print("@echo off", file=file)
                print(f'"{sys.executable}" -m onc.fix_nif_console %*', file=file)
        else:
            path = os.path.join(wrapper_dir, wrapper_name)
            with open(path, "w") as file:
                print("#!/bin/sh ", file=file)
                print(f'"{sys.executable}" -m onc.fix_nif_console "$@"', file=file)
        make_exec(path)
