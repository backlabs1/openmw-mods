# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Natural Character Growth and Decay"
    DESC = "A leveling mod that makes your attributes grow as your skills increase"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/44967
        https://modding-openmw.com/mods/natural-character-growth-and-decay-mw/
    """
    LICENSE = "attribution"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        || (
            ~gameplay-advancement/mbsp-2.1[ncgd]
            >=gameplay-advancement/mbsp-2.2
        )
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        prompt? (
            https://modding-openmw.com/files/NCGD-3.2.zip -> {P}.zip
        )
        !prompt? (
            https://modding-openmw.com/files/ncgdMW-SlowGainFastDecay-3.2.zip
            -> {P}-slowgain.zip
        )
    """
    IUSE = "prompt"
    INSTALL_DIRS = [
        InstallDir(
            "NCGD-3.2",
            PLUGINS=[File("ncgdMW-3.2.omwaddon")],
            S=P,
            REQUIRED_USE="prompt",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("ncgdMW-SlowGainFastDecay-3.2.omwaddon")],
            S=f"{P}-slowgain",
            REQUIRED_USE="!prompt",
        ),
    ]
