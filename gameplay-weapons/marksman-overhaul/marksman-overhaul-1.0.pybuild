# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import P

from common.mw import MW, File, InstallDir
from common.util import CleanPlugin


class Package(CleanPlugin, MW):
    NAME = "Marksman Overhaul"
    DESC = "Adds new bows and crossbows, and rebalances Morrowind's default bow stats"
    HOMEPAGE = "https://web.archive.org/web/20161103115849/http://mw.modhistory.com/download--13282"
    # Allows use of new weapons with attribution, but it is unclear if this applies to
    # the mod as a whole
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    DEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = f"https://web.archive.org/web/20161103115849/http://mw.modhistory.com/file.php?id=13282 -> {P}.rar"
    TEXTURE_SIZES = "256"
    RESTRICT = "mirror"
    IUSE = "minimal devtools"

    INSTALL_DIRS = [
        # Texture Size: 256
        InstallDir(
            "Xenn's Marksman Overhaul",
            PLUGINS=[
                File("Xenn's Marksman Overhaul.ESP", REQUIRED_USE="!minimal"),
                File("Xenn's Marksman Overhaul - Vanilla.ESP", REQUIRED_USE="minimal"),
                File(
                    "Xenn's Marksman Overhaul - Bow Resource.ESP",
                    REQUIRED_USE="devtools",
                ),
            ],
        )
    ]

    def src_prepare(self):
        if "minimal" in self.USE:
            self.clean_plugin(
                "Xenn's Marksman Overhaul/Xenn's Marksman Overhaul - Vanilla.ESP"
            )
        else:
            self.clean_plugin("Xenn's Marksman Overhaul/Xenn's Marksman Overhaul.ESP")
        if "devtools" in self.USE:
            self.clean_plugin(
                "Xenn's Marksman Overhaul/Xenn's Marksman Overhaul - Bow Resource.ESP"
            )
