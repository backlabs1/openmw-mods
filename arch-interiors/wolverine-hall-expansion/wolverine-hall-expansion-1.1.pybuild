# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Wolverine Hall Interior Expansion"
    DESC = "A relatively small, lore-friendly expansion to Wolverine Hall"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/44965"
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation"
    NEXUS_URL = HOMEPAGE
    RDEPEND = """
        base/morrowind
        sadrith-mora-expanded? (
            arch-towns/sadrith-mora-expanded
        )
        !sadrith-mora-expanded? (
            !!arch-towns/sadrith-mora-expanded
        )
        tlad? ( gameplay-misc/true-lights-and-darkness )
    """
    IUSE = "tlad sadrith-mora-expanded"
    SRC_URI = """
        Wolverine_Hall_Interior_Expansion_1-1-44965-1-1.7z
        tlad? (
            !sadrith-mora-expanded? (
                Wolverine_Hall_Interior_Expansion_TLaD_1.1-44965-1-1.7z
            )
            sadrith-mora-expanded? (
                Wolverine_Hall_Interior_Expansion_(No_Legion_HQ_and_TLaD)-44965-1-1.7z
            )
        )
    """
    REQUIRED_USE = "sadrith-mora-expanded? ( tlad )"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            S="Wolverine_Hall_Interior_Expansion_1-1-44965-1-1",
            PLUGINS=[File("Wolverine Hall Interior Expanded.ESP")],
            REQUIRED_USE="!tlad !sadrith-mora-expanded",
        ),
        InstallDir(
            "Data Files",
            S="Wolverine_Hall_Interior_Expansion_TLaD_1.1-44965-1-1",
            PLUGINS=[File("Wolverine Hall Interior Expanded.ESP")],
            REQUIRED_USE="tlad !sadrith-mora-expanded",
        ),
        InstallDir(
            "Data Files",
            S="Wolverine_Hall_Interior_Expansion_(No_Legion_HQ_and_TLaD)-44965-1-1",
            PLUGINS=[File("Wolverine Hall Interior Expanded.ESP")],
            REQUIRED_USE="sadrith-mora-expanded tlad",
        ),
    ]
