# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Dunmer Lanterns Replacer"
    DESC = "Replaces the Dunmer lanterns from morrowind with smooth, detailed versions"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43219"
    # Note: We handle this in an unintuitive manner, however the README states that
    # Derivatives are allowed under the conditions of attribution and non-commercial use
    LICENSE = "attribution-nc free-derivation"
    KEYWORDS = "openmw"
    SRC_URI = "Dunmer_Lanterns_Replacer-43219-12-1-1600742144.7z"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43219"
    TIER = 1
    IUSE = "textures-pherim textures-swg minimal tr"
    RDEPEND = """
        tr? ( landmasses/tamriel-rebuilt )
        !tr? ( !!landmasses/tamriel-rebuilt )
    """
    REQUIRED_USE = "?? ( textures-pherim textures-swg )"
    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir(
            "01 Ashlander Lanterns Retexture 2/Data Files",
            REQUIRED_USE="textures-swg",
        ),
        InstallDir(
            "01 Ashlander Lanterns Retexture 1/Data Files",
            REQUIRED_USE="textures-pherim",
        ),
        InstallDir("02 Ashlander Lantern (Smoothed Only)", REQUIRED_USE="minimal"),
        InstallDir("03 Glow Effect", REQUIRED_USE="!minimal"),
        InstallDir("04 Tamriel Rebuilt Patch", REQUIRED_USE="tr minimal"),
        InstallDir(
            "04 Tamriel Rebuilt Patch - Glow Effect", REQUIRED_USE="tr !minimal"
        ),
    ]
