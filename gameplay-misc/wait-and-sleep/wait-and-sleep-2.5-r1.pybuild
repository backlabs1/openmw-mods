# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from glob import glob

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Wait and Sleep"
    DESC = "More immersive wait and sleep."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45198"
    NEXUS_SRC_URI = """
        !cot? (
            https://www.nexusmods.com/morrowind/mods/45198?tab=files&file_id=1000018349
            -> Wait_and_Sleep_for_OpenMW_-_Standard_Version-45198-2-4-1588589143.7z
        )
        cot? (
            https://www.nexusmods.com/morrowind/mods/45198?tab=files&file_id=1000023522
            -> Wait_and_Sleep_for_OpenMW_-_Cot_Version-45198-2-5-1616529702.7z
        )
    """
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "openmw"
    IUSE = "cot"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("Wait and Sleep for OpenMW - Standard Version.omwaddon")],
            S="Wait_and_Sleep_for_OpenMW_-_Standard_Version-45198-2-4-1588589143",
            REQUIRED_USE="!cot",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Wait and Sleep for OpenMW - Cot Version.omwaddon")],
            S="Wait_and_Sleep_for_OpenMW_-_Cot_Version-45198-2-5-1616529702",
            REQUIRED_USE="cot",
        ),
    ]

    def src_prepare(self):
        # Fix unicode characters, which aren't supported on all filesystems/platforms
        # No plugin is known to refer to this file in its master list,
        # so this shouldn't break anything.
        for file in glob("*.omwaddon"):
            os.rename(file, file.replace("±", "-"))
