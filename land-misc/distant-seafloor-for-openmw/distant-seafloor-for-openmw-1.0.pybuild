# Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    DESC = "Extends the seafloor around Vvardenfell to hide the edge of the world in OpenMW"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/50796"
    KEYWORDS = "openmw"
    LICENSE = "attribution-nc"
    NAME = "Distant Seafloor for OpenMW"
    RDEPEND = "base/morrowind"
    IUSE = "tr"
    TIER = -1
    NEXUS_SRC_URI = """
        https://nexusmods.com/morrowind/mods/50796?tab=files&file_id=1000029393 -> Distant_Seafloor_1.00-50796-1-00-1645291947.zip
        tr? ( https://nexusmods.com/morrowind/mods/50796?tab=files&file_id=1000029394 -> Distant_Seafloor_1.00_AllTamriel-50796-1-00-1645292058.zip )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Distant_Seafloor_1.00",
            S="Distant_Seafloor_1.00-50796-1-00-1645291947",
            PLUGINS=[File("distant_seafloor_1.00.esm")],
            REQUIRED_USE="!tr",
        ),
        InstallDir(
            "Distant_Seafloor_1.00_AllTamriel",
            S="Distant_Seafloor_1.00_AllTamriel-50796-1-00-1645292058",
            PLUGINS=[File("distant_seafloor_1.00_allTamriel.esm")],
            REQUIRED_USE="tr",
        ),
    ]
