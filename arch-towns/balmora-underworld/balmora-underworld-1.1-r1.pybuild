# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod
from common.util import CleanPlugin


class Package(NexusMod, CleanPlugin, MW):
    NAME = "Balmora Underworld"
    DESC = "Adds and improves underground areas in and near Balmora"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/42448"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/42448?tab=files&file_id=1000000213
        -> Balmora_Underworld-42448-V1-1.rar
    """
    SRC_URI = """
       https://modding-openmw.com/files/BalmoraUnderworldNavPointFix.zip
       -> BalmoraUnderworldNavPointFix.zip
    """
    INSTALL_DIRS = [
        InstallDir(
            "Balmora Underworld V1.1",
            S="Balmora_Underworld-42448-V1-1",
            PLUGINS=[File("FLG - Balmora's Underworld V1.1.esp", CLEAN=True)],
        ),
        InstallDir(
            "BalmoraUnderworldNavPointFix",
            S="BalmoraUnderworldNavPointFix",
            PLUGINS=[
                File("BU_pgrd.esp", OVERRIDES=["FLG - Balmora's Underworld V1.1.esp"])
            ],
        ),
    ]
